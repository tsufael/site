---
title:  "Beep"
image:  "beep"
ref: "beep"
categories: "projects.md"
id: 5
---

# Description

IRC bot framework in C++ and libircclient

# Recommendations

* C++
* git
* OO programming

# Contributors

Renato

# Mentors

Renato

# URLs

* [beep](https://github.com/RenatoGeh/beep)
